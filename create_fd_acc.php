<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>CREATE FD ACCOUNT</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-3 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-9">

		<?php



if(isset($_POST['submit']))
{


 	$id=$_SESSION['user_id'];
 	$dep_amt=$_POST['dep_amt'];
 	$fd_tenure=$_POST['fd_tenure'];
  $startdate=date('Y-m-d');


    if($fd_tenure<=24)
    {
      $fd_interest=6;
    }
    else
    {
      $fd_interest=6.5;
    }
    $maturityamt= round($dep_amt * pow(1 + ($fd_interest/100),$fd_tenure/12), 2); 
    $maturitydate= date("Y-m-d", strtotime(" +$fd_tenure months"));
    $pay_detail=$_POST['pay_detail'];
    $updatedate=date('Y-m-d');

    $cnt="select COUNT(acc_id) from user_account";

if($cnt>0)
{
  $sql="SELECT FLOOR(RAND() * 99999) AS random_num FROM user_account WHERE 'random_num' NOT IN (SELECT acc_no FROM user_account) LIMIT 1";//only work select 
  $fd_accno=ReturnAnyValue($conn,$sql);
 }
 else
 {
 	$fd_accno=rand(9999,1000000);

 }


/*$Present_Value = 100000; 
$Int_Rate = 8; 
$Num_Periods = 6; 

$Future_Value = 
echo "$Present_Value @ $Int_Rate % over $Num_Periods periods becomes $Future_Value"; */


  $sql="insert into user_account(user_id,acc_type,acc_no,fd_amt,maturity_amt,interest_rate,start_date,tenure,maturity_date,cur_bal,update_date) values ('$id','1','$fd_accno','$dep_amt','$maturityamt','$fd_interest','$startdate','$fd_tenure','$maturitydate','$dep_amt','$updatedate')";

  

  if(mysqli_query($conn,$sql))
  {
  	
	
	 echo " FD Account Created Successfully !!";
	 echo "<br> FD Account Number- ".$fd_accno;
   echo "<br>Maturity Date: ".$maturitydate;
  echo "<br>Maturity Amount: ".$maturityamt;
  
  }
  else
    echo "error:".$sql."<br>".mysqli_error($conn);
}
else
{
?>  
  

  <form method="post" name="myform" action="">  

  	<div class="form-group">
    <label for="dep_amt">Amount: </label>
    <input name="dep_amt" type="number" class="form-control"  id="dep_amt" required>
  </div>


  	<div class="form-group">
      <label for="fd_tenure">FD Tenure: </label>

      <select name="fd_tenure" required>
    <?php 

    for($i=1; $i<=20; $i++)
     {
     		$s=3;
     		$p=$s*$i;
      ?>

     <option value="<?php echo $p;?>"><?php echo $p;?> months</option>
   <?php
       }
       ?>
 <option name="fd_tenure"> </option>   
    </select> 

	</div>


*** If FD tenure is less than or equal to 2 years then Interest Rate is 6% otherwise 6.5% ***
	
 
      <button name="submit" type="submit" class="btn btn-primary btn-sm">Create Account</button>
  </form>

<?php
}
?>


	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


