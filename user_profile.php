<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>MY PROFILE</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-3 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-9">

		<?php

if(isset($_POST['submit']))
{

 	$id=$_SESSION['user_id'];
 	$dob=$_POST['dob'];
	$city=$_POST['city'];
	$state=$_POST['state'];
	$address=$_POST['address'];
	$pin=$_POST['pincode'];
	$cname=$_POST['company_name'];
	$oaddr=$_POST['office_addr'];
	$jobtype=$_POST['job_type'];
	$udate = date("Y-m-d");
	


 $sql="update user_profile set  dob='$dob',city='$city',state='$state',address='$address',pincode='$pin',company_name='$cname',office_addr='$oaddr',job_type='$jobtype',update_date='$udate' where user_id=$id";

 if(mysqli_query($conn,$sql))
 {
 	echo "Updated Successfully !!!";
 	
 }
 else
 {
 	echo "There was some error!!";
 }
}
else
{
	$sql="select * from user_profile where user_id=".$_SESSION['user_id'];
	
$rs=mysqli_query($conn,$sql);
$row=mysqli_fetch_array($rs);
?>
 <form method="post" name="myform" action="" enctype="multipart/form-data"s> 
 		

  <div class="form-group">
    <label for="dob">Date of birth:  </label>
    <input name="dob" type="Date" class="form-control"  id="dob" value="<?php echo $row['dob'];?>" required>
  </div>

  <div class="form-group">
    <label for="city">City:  </label>
    <input name="city" type="text" class="form-control" id="city"value="<?php echo $row['city'];?>" required>
  </div>

<div class="form-group">
    <label for="state">State:  </label>
    <input name="state" type="text" class="form-control" id="state"value="<?php echo $row['state'];?>" required>
  </div>

<div class="form-group">
  <label for="address">Address:</label>
  <textarea name="address" class="form-control" rows="4" id="address" required><?php echo $row['address'];?></textarea>
</div>

<div class="form-group">
    <label for="pincode">Pin Code:  </label>
    <input name="pincode" type="number" class="form-control" id="pincode"value="<?php echo $row['pincode'];?>" required>
  </div>

  <div class="form-group">
    <label for="company_name">Company Name:  </label>
    <input name="company_name" type="text" class="form-control" id="company_name" value="<?php echo $row['company_name'];?>" required>
  </div>

  <div class="form-group">
  <label for="office_addr">Office Address:</label>

  <textarea class="form-control" name="office_addr" rows="4" id="office_addr" required><?php echo $row['office_addr'];?></textarea>
</div>

 <div class="form-group">
    <label for="job_type">Job Type:  </label>
    <input name="job_type" type="text" class="form-control" id="job_type"value="<?php echo $row['job_type'];?>" required>
  </div>


  
    <button name="submit" type="submit" class="btn btn-primary btn-sm">Update</button>
  </form>

	
 <?php
 }

 ?>
	</div>
</div>


<div class="row">

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>

</div>
</div>
</div>
</body>
</html>

