<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>REQUEST LOAN</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-3 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-9">

	<?php



if(isset($_POST['submit']))
{


 	$uid=$_SESSION['user_id'];
 	$loan_amt=$_POST['loan_amt'];
  $applydate=date('Y-m-d');
  $tenure=$_POST['tenure'];
  

   // EMI Calculator program in PHP 
  
// Function to calculate EMI 
function emi_calculator($p, $r, $t) 
{ 
    $emi; 
  
    // one month interest 
    $r = $r / (12 * 100); 
      
    // one month period 
    $t = $t * 12;  
      
    $emi = ($p * $r * pow(1 + $r, $t)) /  
                  (pow(1 + $r, $t) - 1); 
  
    return ($emi); 
} 
 
   // Driver Code 
    $principal = $loan_amt; // 2 lakh 50 thousands as principal
    $rate = 18; // 9.25 as Rate of interest per annum
    $time = $tenure/12; // 2 years as Repayment period
    $emi = emi_calculator($principal, $rate, $time); //Reducing Rate of interest


  $sql="insert into loan_application(user_id,loan_amt,emi_amt,tenure,apply_date) values ('$uid','$loan_amt','$emi','$tenure','$applydate')";

  if(mysqli_query($conn,$sql))
  {
  	 
		echo "Loan Request Sent!!";
		echo "<br>Loan Amount: ".$loan_amt; 
		echo "<br>Rate of Interest: ".$rate; 
		echo "<br>Tenure: ".$tenure."<br>"; 
		echo "<br>Monthly EMI is: ".round($emi); 

	 
  
  }
  else
    echo "error:".$sql."<br>".mysqli_error($conn);
}
else
{
?>  
  

  <form method="post" name="myform" action="">  

  	<div class="form-group">
    <label for="loan_amt">Loan Amount:  </label>
    <input name="loan_amt" type="number" class="form-control" id="loan_amt" required>
  </div>

  <div class="form-group">
      <label for="tenure"> Tenure: </label>

      <select name="tenure">
    <?php 

    for($i=1; $i<=20; $i++)
     {
        $s=6;
        $p=$s*$i;
      ?>

     <option value="<?php echo $p;?>"><?php echo $p;?> months</option>
   <?php
       }
       ?>
 <option name="tenure"> </option>   
    </select> 

	
 
      <button name="submit" type="submit" class="btn btn-primary btn-sm">Submit Request</button>
  </form>

<?php
}
?>

	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


