<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>LOAN REQUESTS</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-3 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-9">

<form class="form-inline" name=f1 method=post action=''>

    <div class="form-group">
    <label for="status">Status:  </label>
  <select  name="status" class="form-group mr-2" id="status">
    <option value="1">Approved</option>
    <option value="2">Request Pending</option>
    <option value="0">Rejected</option>
  </select>
  </div>

    <button name="submit" type="submit" class="btn btn-primary btn-sm"> Search</button>


</form>

		<?php

    if(isset($_POST['submit']))

  {

    $status=$_POST['status'];

    $str="";

      if($status!="")
          {
            $str=" m.status = '$status' and ";
          }
  




  $sql="select m.*,u.name from loan_application m, users u where".$str." m.user_id=u.user_id order by `app_id` DESC ";

}
  else
{

  $sql="select m.*,u.name from loan_application m, users u where m.user_id=u.user_id order by `app_id` DESC ";

}




$rs=mysqli_query($conn,$sql);

echo "<br>";

?>
<div class="table-responsive-sm">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Loan Amount</th>
        <th>Tenure</th>
        <th>Interest Rate</th>
        <th>Apply Date</th>
        <th>Response Date</th>
        <th>Action</th>
        <th>Loan Disbursement</th>

      </tr>
    </thead>
    <tbody>


<?php

$i=1;
while($row=mysqli_fetch_array($rs))

{

  

  echo "<tr>";
 	

 	echo "<td>$i</td>";
  echo "<td>".$row['name']."</td>";
	echo "<td>".$row['loan_amt']."</td>";
	echo "<td>".$row['tenure']." months</td>";
	echo "<td>".$row['interest_rate']."%</td>";
	echo "<td>".$row['apply_date']."</td>";
  if ($row['approval_date']==NULL)
    echo "<td>NA</td>";
  else
    echo "<td>".$row['approval_date']."</td>";

  $status=$row['status'];
  if($status==0) $strStatus="Rejected";
  if($status==1) $strStatus="Approved";
  if($status==2)
    echo "<td>"."<a href=loan_approve.php?appID=".$row['app_id'].">Approve</a>"." | "."<a href=loan_decline.php?appID=".$row['app_id'].">Decline</a>"."</td>";	
  else
    echo "<td>".$strStatus."</td>";


  if($strStatus=="Approved")
  {
            $disperce=$row['Disburse'];
            if($disperce==0)
            {
              echo "<td>"."<a href=disburse.php?accno=".$row['acc_no'].">Disburse</a>"."</td>";  
            }
            if($disperce==1)
            {
              echo "<td>Disbursed</td>";
            }
  }

  else
    echo "<td>NA</td>";
   

      

      
   
$i=$i+1;
  echo "</tr>";
  
}
  ?>
</tbody>
</table>
</div>


        
	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


