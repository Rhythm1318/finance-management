<?php  include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>CONFIRM PAYMENT</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-3 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-9">

		
		<?php

if(isset($_POST['submit']))
{

 	$accno=$_POST['acc_no'];
	$payamt=$_POST['pay_amt'];
	$paymethod=$_POST['pay_method'];
	$paydetail=$_POST['pay_detail'];
	$paydate = date("Y-m-d");
	$prevB=$_POST['prev_bal'];
	$curB=$_POST['cur_bal'];
	$acc_type=$_POST['acc_type'];

	$sql="select COUNT(*) from user_payment where acc_no=$accno";
	$cnt=ReturnAnyValue($conn,$sql);

	if($cnt>0)
	{
		$Ssql="select cur_bal from user_payment where acc_no=$accno order by pay_id DESC LIMIT 0,1";
		$curB=ReturnAnyValue($conn,$Ssql);
		$prevB=$curB;
		$curB=$prevB+$payamt;
	}

	else
	{
		$prevB=0;
		$curB=$prevB+$payamt;

	}

	$sql="insert into user_payment(acc_no,pay_amt,acc_type,pay_method,pay_detail,pay_date,prev_bal,cur_bal) values ('$accno','$payamt','$acc_type','$paymethod','$paydetail','$paydate','$prevB','$curB')";



  if(mysqli_query($conn,$sql))
  {
  	
	
	 echo " Payment Updated Successfully !!";

	 echo "<br> Account Number- ".$accno;
	 echo "<br> Current Balance- ".$curB;

	 $sql="update user_account set cur_bal=$curB where acc_no=$accno";
	 mysqli_query($conn,$sql);



	 $Ssql="select cur_bal from society_account order by trans_id DESC LIMIT 0,1";
		$curB=ReturnAnyValue($conn,$Ssql);
		$prevB=$curB;
		$curB=$prevB+$payamt;

$dt=date('Y-m-d');

$login_id=$_SESSION['admin_id'];

$sql="insert into society_account(trans_date,acc_no,trans_amt,trans_type,trans_detail,acc_type,prev_bal,cur_bal,update_date,update_by,status) 
	 							values ('$dt','$accno','$payamt','1','$paydetail','$acc_type','$prevB','$curB','$dt','$login_id','1')";

mysqli_query($conn,$sql);

  }
  else
    echo "error:".$sql."<br>".mysqli_error($conn);
}
else
{

	$accNO=$_POST['acc_no'];

	$sql="select * from user_account where acc_no=".$accNO;

	$rs=mysqli_query($conn,$sql);
	

	$row=mysqli_fetch_array($rs);

	$cnt=mysqli_num_rows($rs);

	if($cnt==0)
	{
		$msg="Sorry Invalid Account Number !";
		$url="prepayment.php";
		 dispMessage($msg,$url);
	}


	$accType=$row['acc_type'];
	if($accType==1)  $amount=$row['fd_amt'];
	if($accType==2)  $amount=$row['monthly_amt'];
	if($accType==3)  $amount=$row['emi_amt'];


	$sql="select name from users where user_id=".$row['user_id'];

	$memName=ReturnAnyValue($conn,$sql);

?>
 <form  method="post" name="myform" action="" enctype="multipart/form-data"> 
 	<input type="hidden" name="acc_no" id="acc_no" value=<?php echo $accNO;?>>
 	<input type="hidden" name="acc_type" id="acc_type" value=<?php echo $accType;?>>
 	<input type="hidden" name="pay_amt" id="pay_amt" value=<?php echo $amount;?>>

 	
 	<div class ="form-group">
 		<label for="acc_type">Account Type:  <?php  if($accType==1) echo "FD Account";
 													if($accType==2) echo "RD Account";
 													if($accType==3) echo "Loan Account"; ?>

  <div class="form-group">
    Account Number:  <?php echo $accNO; ?>
  </div>

   <div class="form-group">
   Member Name:  <?php echo $memName;?>

  </div>


  <div class="form-group">
    Payment Amount: <?php echo $amount; ?>
  </div>

 

<div class="form-group">
    <label for="pay_method">Payment Method:  </label>
  <select  name="pay_method" class="form-group" id="pay_method">
    <option value="1">Cash</option>
    <option value="2">Cheque</option>
    <option value="3">Online Transfer</option>
  </select>
  </div>

<div class="form-group">
    <label for="pay_detail">Payment Detail:  </label>
  <textarea class="form-control" name="pay_detail" rows="4" id="pay_detail" required></textarea>
</div>

  
    <button name="submit" type="submit" class="btn btn-primary btn-sm">Add Payment</button>
  </form>

	
 <?php
 }

 ?>
	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


