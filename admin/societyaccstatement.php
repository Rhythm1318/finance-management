<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>BANK STATEMENTS</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-2 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-10">

	<?php





   $sql="SELECT * FROM `society_account` ORDER BY `trans_id` DESC";

  
$rs=mysqli_query($conn,$sql);

echo "<br>";

?>
<div class="table-responsive-sm">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
       <th>Account Number</th>
            <th>Account Type</th>
            <th>Transaction Date</th>
            <th>Transaction Type</th>
            <th>Transaction Detail</th>
            <th>Transaction Amount</th>
            <th>Previous Balance</th>
            <th>Current Balance</th>
            <th>Update Date</th>

      </tr>
    </thead>
    <tbody>


<?php

$i=1;
while($row=mysqli_fetch_array($rs))

{
  echo "<tr>";
  

  echo "<td>$i</td>";
  echo "<td>".$row['acc_no']."</td>";

  $acctype=$row['acc_type'];
    if($acctype==1) $acctype="FD Account";
    if($acctype==2) $acctype="RD Account";
    if($acctype==3) $acctype="Loan Account";
  echo "<td>".$acctype."</td>";

  echo "<td>".$row['trans_date']."</td>";

  $transtype=$row['trans_type'];
    if($transtype==1) $transtype="Credit";
    if($transtype==2) $transtype="Debit";
  echo "<td>".$transtype."</td>";

  echo "<td>".$row['trans_amt']."</td>";
  echo "<td>".$row['trans_detail']."</td>";
  echo "<td>".round($row['prev_bal'],2)."</td>";
  echo "<td>".round($row['cur_bal'],2)."</td>";
  echo "<td>".$row['update_date']."</td>";


$i=$i+1;
  echo "</tr>";
  
}
  ?>
</tbody>
</table>
</div>


	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


