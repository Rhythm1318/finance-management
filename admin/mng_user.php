<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>MANAGE USERS</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-2 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-10">

		<?php


$sql="select * from users ORDER BY `users`.`user_id` DESC";

$rs=mysqli_query($conn,$sql);


?>
<div class="table-responsive-sm" >
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>E-mail id</th>
        <th>Last Login</th>
        <th>Login IP</th>
        <th>Action</th>
        <th>Status</th>

      </tr>
    </thead>
    <tbody>


<?php

$i=1;
while($row=mysqli_fetch_array($rs))

{

  echo "<tr>";
 	echo "<td>$i</td>";
  echo "<td>"."<a href=view_user_profile.php?userID=".$row['user_id'].'>'.$row['name']."</a></td>";
  echo "<td>".$row['email']."</td>";
	echo "<td>".$row['last_activity']."</td>";
	echo "<td>".$row['login_ip']."</td>";
  echo "<td>"."<a href=user_delete.php?userID=".$row['user_id'].">Delete</a>"." | "."<a href=edituserdetail.php?userID=".$row['user_id'].">Edit</a>"."</td>";

$status=$row['status'];
if($status==0) $strStatus="<a href=useractive.php?userID=".$row['user_id'].">Activate User</a>";
if($status==1) $strStatus="<a href=usernotactive.php?userID=".$row['user_id'].">Deactivate User</a>";
echo "<td>".$strStatus."</td>";


  
   
$i=$i+1;
  echo "</tr>";
  
}
  ?>
</tbody>
</table>
</div>


        
	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


