-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2020 at 06:55 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finance`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `number` int(11) DEFAULT NULL,
  `admin_type` tinyint(4) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` datetime DEFAULT NULL,
  `login_ip` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `number`, `admin_type`, `email`, `password`, `last_activity`, `login_ip`, `status`) VALUES
(1, 'rhythm', 1234567890, 1, 'rhythm@admin.com', '1234', '2020-05-23 00:11:20', '::1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `loan_application`
--

CREATE TABLE `loan_application` (
  `app_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `acc_no` int(11) NOT NULL DEFAULT '0',
  `loan_amt` double NOT NULL,
  `emi_amt` double NOT NULL DEFAULT '0',
  `tenure` int(11) NOT NULL,
  `interest_rate` double NOT NULL DEFAULT '18',
  `apply_date` date NOT NULL,
  `approval_date` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT '2',
  `disburse` tinyint(4) NOT NULL DEFAULT '0',
  `disburse_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `loan_application`
--

INSERT INTO `loan_application` (`app_id`, `user_id`, `acc_no`, `loan_amt`, `emi_amt`, `tenure`, `interest_rate`, `apply_date`, `approval_date`, `status`, `disburse`, `disburse_date`) VALUES
(10, 1, 0, 10000, 916.79992906229, 12, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(11, 1, 0, 3000, 191.41734529564, 18, 18, '2020-05-11', '2020-05-11', 0, 1, '2020-05-17'),
(12, 1, 0, 12, 2.1063025755446, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(13, 1, 0, 1, 0.17552521462872, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(14, 1, 0, 5588848, 980983.74472727, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(15, 1, 0, 5444, 955.55926843873, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(16, 1, 0, 6780, 1190.0609551827, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(17, 1, 0, 400, 70.210085851486, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(19, 3, 0, 123456, 21669.640897203, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(20, 1, 0, 550404, 96609.780232504, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(21, 1, 0, 4500, 789.86346582922, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(22, 8, 0, 30000, 1084.5718660775, 36, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(23, 8, 0, 34, 5.9678572973763, 6, 18, '2020-05-11', '2020-05-11', 1, 1, '2020-05-17'),
(24, 1, 0, 10000, 1755.2521462872, 6, 18, '2020-05-12', '2020-05-12', 1, 1, '2020-05-17'),
(25, 9, 0, 3000, 191.41734529564, 18, 18, '2020-05-12', '2020-05-12', 1, 1, '2020-05-17'),
(26, 9, 0, 7000, 1228.676502401, 6, 18, '2020-05-12', '2020-05-12', 1, 1, '2020-05-17'),
(28, 11, 903714, 7000, 1228.676502401, 6, 18, '2020-05-13', '2020-05-13', 1, 0, '2020-05-13'),
(29, 1, 217840, 890000, 156217.44101956, 6, 18, '2020-05-13', '2020-05-13', 1, 0, '2020-05-13'),
(31, 1, 0, 123, 21.589601399332, 6, 18, '2020-05-14', '2020-05-15', 0, 1, '2020-05-17'),
(32, 1, 0, 12020, 2109.8130798372, 6, 18, '2020-05-15', '2020-05-15', 0, 1, '2020-05-17'),
(33, 1, 285292, 12345, 2166.8587745915, 6, 18, '2020-05-16', '2020-05-16', 1, 0, NULL),
(34, 1, 420688, 500, 87.762607314358, 6, 18, '2020-05-17', '2020-05-21', 1, 1, '2020-05-21'),
(35, 3, 0, 12000, 765.66938118256, 18, 18, '2020-05-17', '2020-05-21', 0, 1, '2020-05-17'),
(36, 1, 235176, 3000, 275.03997871869, 12, 18, '2020-05-20', '2020-05-21', 1, 1, '2020-05-21'),
(37, 1, 0, 123, 21.589601399332, 6, 18, '2020-05-21', '2020-05-21', 0, 0, NULL),
(38, 1, 0, 1234, 113.13311124629, 12, 18, '2020-05-21', NULL, 2, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `society_account`
--

CREATE TABLE `society_account` (
  `trans_id` int(11) NOT NULL,
  `trans_date` date NOT NULL,
  `acc_no` int(11) NOT NULL,
  `trans_amt` double NOT NULL,
  `trans_type` tinyint(4) NOT NULL,
  `trans_detail` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `acc_type` tinyint(4) NOT NULL,
  `prev_bal` double NOT NULL,
  `cur_bal` double NOT NULL,
  `update_date` date NOT NULL,
  `update_by` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `society_account`
--

INSERT INTO `society_account` (`trans_id`, `trans_date`, `acc_no`, `trans_amt`, `trans_type`, `trans_detail`, `acc_type`, `prev_bal`, `cur_bal`, `update_date`, `update_by`, `status`) VALUES
(1, '2020-05-04', 0, 10000, 1, 'initial amt', 0, 0, 10000, '2020-05-04', 0, 1),
(6, '2020-05-08', 946314, 1000, 1, 'cash', 2, 10000, 11000, '2020-05-08', 1, 1),
(7, '2020-05-08', 946314, 1000, 1, 'cash', 2, 11000, 12000, '2020-05-08', 1, 1),
(8, '2020-05-09', 893907, 1000, 1, 'cash', 2, 12000, 13000, '2020-05-09', 1, 1),
(9, '2020-05-11', 983802, 10, 1, 'cash', 3, 13000, 13010, '2020-05-11', 1, 1),
(10, '2020-05-12', 163802, 500, 1, 'cash', 2, 13010, 13510, '2020-05-12', 1, 1),
(11, '2020-05-13', 525388, 96, 2, 'Loan Disburstment by Cash', 3, 13510, 13414, '2020-05-13', 1, 1),
(12, '2020-05-13', 946314, 500, 1, 'cash', 2, 13414, 13914, '2020-05-13', 1, 1),
(13, '2020-05-14', 1, 1, 1, 'X', 1, 13914, 13915, '2020-05-14', 1, 1),
(14, '2020-05-16', 217840, 156217.44101956, 1, 'cash', 3, 13915, 170132.44101956, '2020-05-16', 1, 1),
(15, '2020-05-16', 900168, 1111, 1, 'Cash', 1, 170132.44101956, 171243.44101956, '2020-05-16', 1, 1),
(16, '2020-05-16', 900168, 1111, 1, 'cash', 1, 171243.44101956, 172354.44101956, '2020-05-16', 1, 1),
(18, '2020-05-17', 0, 10000, 2, 'Loan Disburstment by Cash', 3, 172354.44101956, 162354.44101956, '2020-05-17', 1, 1),
(19, '2020-05-20', 614781, 1000, 1, 'cash', 2, 162354.44101956, 163354.44101956, '2020-05-20', 1, 1),
(20, '2020-05-21', 614781, 1000, 1, 'Cash', 2, 163354.44101956, 164354.44101956, '2020-05-21', 1, 1),
(21, '2020-05-21', 235176, 3000, 2, 'Loan Disburstment by Cash', 3, 164354.44101956, 161354.44101956, '2020-05-21', 1, 1),
(22, '2020-05-21', 420688, 500, 2, 'Loan Disburstment by Cash', 3, 161354.44101956, 160854.44101956, '2020-05-21', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sponsor_id` int(11) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` double NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` datetime DEFAULT NULL,
  `login_ip` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `sponsor_id`, `email`, `mobile`, `password`, `last_activity`, `login_ip`, `status`) VALUES
(1, 'Rhythm', NULL, 'rhythm.8@live.com', 3443342334, '1234', '2020-05-23 00:24:38', '::1', 1),
(3, 'Abc', NULL, 'abc@hotmail.com', 5442442422, '1234', '2020-05-20 16:28:25', '::1', 1),
(8, 'alex', NULL, 'alex@1234', 6665554443, '1234', NULL, '', 1),
(9, 'Raj', NULL, 'raj@123', 1929238438, '1234', '2020-05-12 23:55:40', '::1', 1),
(10, 'Raju', NULL, 'raju@123', 4838291838, '1234', '2020-05-13 01:51:35', '::1', 1),
(11, 'kartik', NULL, 'kk@gmail.com', 3929392393, '1234', '2020-05-13 14:19:36', '::1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE `user_account` (
  `acc_id` int(11) NOT NULL,
  `acc_no` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `acc_type` tinyint(4) NOT NULL,
  `monthly_amt` double NOT NULL DEFAULT '0',
  `loan_amt` double NOT NULL DEFAULT '0',
  `fd_amt` double NOT NULL DEFAULT '0',
  `emi_amt` double NOT NULL DEFAULT '0',
  `maturity_amt` double NOT NULL DEFAULT '0',
  `interest_rate` double NOT NULL,
  `start_date` date NOT NULL,
  `tenure` int(11) NOT NULL,
  `maturity_date` date DEFAULT NULL,
  `cur_bal` double NOT NULL DEFAULT '0',
  `update_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`acc_id`, `acc_no`, `user_id`, `acc_type`, `monthly_amt`, `loan_amt`, `fd_amt`, `emi_amt`, `maturity_amt`, `interest_rate`, `start_date`, `tenure`, `maturity_date`, `cur_bal`, `update_date`, `status`) VALUES
(41, 946314, 1, 2, 1000, 0, 0, 0, 1004.9752062727, 6, '2020-05-08', 6, '2020-11-08', 4000, '2020-05-08', 1),
(42, 713096, 1, 1, 0, 0, 100000, 0, 120794.96, 6.5, '2020-05-08', 36, '2023-05-08', 100000, '2020-05-08', 1),
(43, 944409, 1, 2, 2000, 0, 0, 0, 2009.9504125453, 6, '2020-05-08', 6, '2020-11-08', 2000, '2020-05-08', 1),
(44, 216366, 1, 1, 0, 0, 1200000, 0, 1519645.45, 6.5, '2020-05-08', 45, '2024-02-08', 1200000, '2020-05-08', 1),
(45, 893907, 7, 2, 1000, 0, 0, 0, 1004.9752062727, 6, '2020-05-09', 12, '2021-05-09', 1000, '2020-05-09', 1),
(46, 138128, 7, 1, 0, 0, 10000, 0, 10146.74, 6, '2020-05-09', 3, '2020-08-09', 10000, '2020-05-09', 1),
(47, 983802, 1, 3, 0, 400, 0, 70.210085851486, 0, 18, '2020-05-11', 6, NULL, 10, '2020-05-11', 1),
(48, 327594, 1, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-11', 6, '2020-11-11', 500, '2020-05-11', 1),
(49, 838644, 1, 1, 0, 0, 1000, 0, 1014.67, 6, '2020-05-11', 3, '2020-08-11', 1000, '2020-05-11', 1),
(50, 465396, 5, 3, 0, 40595, 0, 7125.4460878527, 0, 18, '2020-05-11', 6, NULL, 0, '2020-05-11', 1),
(51, 583141, 3, 3, 0, 123456, 0, 21669.640897203, 0, 18, '2020-05-11', 6, NULL, 0, '2020-05-11', 1),
(52, 335551, 3, 3, 0, 123456, 0, 21669.640897203, 0, 18, '2020-05-11', 6, NULL, 0, '2020-05-11', 1),
(53, 683268, 1, 3, 0, 550404, 0, 96609.780232504, 0, 18, '2020-05-11', 6, NULL, 0, '2020-05-11', 1),
(54, 845715, 1, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-11', 6, '2020-11-11', 500, '2020-05-11', 1),
(55, 101498, 1, 1, 0, 0, 6788, 0, 6887.61, 6, '2020-05-11', 3, '2020-08-11', 6788, '2020-05-11', 1),
(56, 670899, 1, 3, 0, 4500, 0, 789.86346582922, 0, 18, '2020-05-11', 6, NULL, 0, '2020-05-11', 1),
(57, 959476, 8, 2, 1000, 0, 0, 0, 1004.9752062727, 6, '2020-05-11', 6, '2020-11-11', 1000, '2020-05-11', 1),
(58, 988056, 8, 1, 0, 0, 100000, 0, 109133.68, 6, '2020-05-11', 18, '2021-11-11', 100000, '2020-05-11', 1),
(59, 310124, 8, 3, 0, 30000, 0, 1084.5718660775, 0, 18, '2020-05-11', 36, NULL, 0, '2020-05-11', 1),
(60, 388162, 8, 3, 0, 34, 0, 5.9678572973763, 0, 18, '2020-05-11', 6, NULL, 123, '2020-05-11', 1),
(61, 826909, 1, 3, 0, 10000, 0, 1755.2521462872, 0, 18, '2020-05-12', 6, NULL, 0, '2020-05-12', 1),
(62, 331007, 9, 1, 0, 0, 12345, 0, 12526.15, 6, '2020-05-12', 3, '2020-08-12', 12345, '2020-05-12', 1),
(63, 900168, 9, 1, 0, 0, 1111, 0, 1127.3, 6, '2020-05-12', 3, '2020-08-12', 2222, '2020-05-12', 1),
(64, 163802, 9, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-12', 6, '2020-11-12', 2500, '2020-05-12', 1),
(65, 323434, 9, 3, 0, 3000, 0, 191.41734529564, 0, 18, '2020-05-12', 18, NULL, 0, '2020-05-12', 1),
(66, 392214, 9, 3, 0, 7000, 0, 1228.676502401, 0, 18, '2020-05-12', 6, NULL, 0, '2020-05-12', 1),
(67, 769228, 11, 1, 0, 0, 2000, 0, 2029.35, 6, '2020-05-13', 3, '2020-08-13', 2000, '2020-05-13', 1),
(68, 762284, 11, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-13', 6, '2020-11-13', 500, '2020-05-13', 1),
(69, 525388, 1, 3, 0, 96, 0, 16.850420604357, 0, 18, '2020-05-13', 6, NULL, 0, '2020-05-13', 1),
(70, 903714, 11, 3, 0, 7000, 0, 1228.676502401, 0, 18, '2020-05-13', 6, NULL, 0, '2020-05-13', 1),
(71, 217840, 1, 3, 0, 890000, 0, 156217.44101956, 0, 18, '2020-05-13', 6, NULL, 156217.44101956, '2020-05-13', 1),
(72, 134623, 13, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-14', 6, '2020-11-14', 500, '2020-05-14', 1),
(73, 420955, 13, 3, 0, 12000, 0, 2106.3025755446, 0, 18, '2020-05-14', 6, NULL, 0, '2020-05-14', 1),
(74, 384456, 1, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-16', 6, '2020-11-16', 500, '2020-05-16', 1),
(75, 285292, 1, 3, 0, 12345, 0, 2166.8587745915, 0, 18, '2020-05-16', 6, NULL, 0, '2020-05-16', 1),
(76, 715868, 1, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-20', 6, '2020-11-20', 500, '2020-05-20', 1),
(77, 574373, 1, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-20', 6, '2020-11-20', 500, '2020-05-20', 1),
(78, 253396, 1, 2, 5000, 0, 0, 0, 5024.8760313633, 6, '2020-05-20', 6, '2020-11-20', 5000, '2020-05-20', 1),
(79, 123300, 1, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-20', 6, '2020-11-20', 500, '2020-05-20', 1),
(80, 816463, 1, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-20', 6, '2020-11-20', 500, '2020-05-20', 1),
(81, 198901, 1, 2, 2500, 0, 0, 0, 2512.4380156816, 6, '2020-05-20', 6, '2020-11-20', 2500, '2020-05-20', 1),
(82, 847747, 1, 2, 500, 0, 0, 0, 502.48760313633, 6, '2020-05-20', 6, '2020-11-20', 500, '2020-05-20', 1),
(83, 26759, 1, 1, 0, 0, 1200, 0, 1235.48, 6, '2020-05-20', 6, '2020-11-20', 1200, '2020-05-20', 1),
(84, 842776, 1, 2, 4000, 0, 0, 0, 4019.9008250906, 6, '2020-05-20', 6, '2020-11-20', 4000, '2020-05-20', 1),
(85, 614781, 1, 2, 1000, 0, 0, 0, 1004.9752062727, 6, '2020-05-20', 12, '2021-05-20', 2000, '2020-05-20', 1),
(86, 41245, 1, 1, 0, 0, 15000, 0, 15443.45, 6, '2020-05-20', 6, '2020-11-20', 15000, '2020-05-20', 1),
(87, 235176, 1, 3, 0, 3000, 0, 275.03997871869, 0, 18, '2020-05-20', 12, NULL, 0, '2020-05-21', 1),
(88, 420688, 1, 3, 0, 500, 0, 87.762607314358, 0, 18, '2020-05-17', 6, NULL, 0, '2020-05-21', 1),
(89, 715693, 1, 2, 3000, 0, 0, 0, 3014.925618818, 6, '2020-05-21', 42, '2023-11-21', 3000, '2020-05-21', 1),
(90, 165136, 1, 1, 0, 0, 123455, 0, 125266.56, 6, '2020-05-21', 3, '2020-08-21', 123455, '2020-05-21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_fd_account`
--

CREATE TABLE `user_fd_account` (
  `fd_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fd_accno` int(11) NOT NULL,
  `dep_amt` double NOT NULL,
  `fd_interest` double NOT NULL,
  `fd_tenure` int(11) NOT NULL,
  `startdate` date NOT NULL,
  `maturitydate` date NOT NULL,
  `maturity_amt` double NOT NULL DEFAULT '0',
  `pay_detail` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `update_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_fd_account`
--

INSERT INTO `user_fd_account` (`fd_id`, `user_id`, `fd_accno`, `dep_amt`, `fd_interest`, `fd_tenure`, `startdate`, `maturitydate`, `maturity_amt`, `pay_detail`, `update_date`, `status`) VALUES
(7, 4, 1998859286, 1000, 6, 6, '2020-05-03', '2020-11-03', 1029.56, 'Cash', '2020-05-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_payment`
--

CREATE TABLE `user_payment` (
  `pay_id` int(11) NOT NULL,
  `acc_no` int(11) NOT NULL,
  `pay_amt` double NOT NULL,
  `acc_type` tinyint(4) NOT NULL,
  `pay_method` tinyint(4) NOT NULL,
  `pay_detail` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pay_date` date NOT NULL,
  `prev_bal` float NOT NULL DEFAULT '0',
  `cur_bal` double NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_payment`
--

INSERT INTO `user_payment` (`pay_id`, `acc_no`, `pay_amt`, `acc_type`, `pay_method`, `pay_detail`, `pay_date`, `prev_bal`, `cur_bal`, `status`) VALUES
(24, 946314, 1000, 2, 1, 'cash', '2020-05-08', 0, 1000, 1),
(25, 946314, 1000, 2, 1, 'cash', '2020-05-08', 1000, 2000, 1),
(26, 946314, 1000, 2, 1, 'cash', '2020-05-08', 2000, 3000, 1),
(27, 893907, 1000, 2, 1, 'cash', '2020-05-09', 0, 1000, 1),
(28, 983802, 10, 3, 1, 'cash', '2020-05-11', 0, 10, 1),
(32, 163802, 500, 2, 1, 'cash', '2020-05-12', 0, 500, 1),
(33, 163802, 500, 2, 1, 'cash', '2020-05-12', 500, 1000, 1),
(34, 163802, 500, 2, 1, 'Cash Deposit', '2020-05-12', 1000, 1500, 1),
(35, 163802, 500, 2, 1, 'Cash Dep', '2020-05-12', 1500, 2000, 1),
(36, 163802, 500, 2, 1, 'cash', '2020-05-12', 2000, 2500, 1),
(37, 946314, 500, 2, 1, 'cash', '2020-05-13', 3000, 3500, 1),
(38, 946314, 500, 2, 1, 'cash', '2020-05-13', 3500, 4000, 1),
(40, 217840, 156217.44101956, 3, 1, 'cash', '2020-05-16', 0, 156217.44101956, 1),
(41, 900168, 1111, 1, 1, 'Cash', '2020-05-16', 0, 1111, 1),
(44, 614781, 1000, 2, 3, 'cash', '2020-05-20', 0, 1000, 1),
(45, 614781, 1000, 2, 1, 'Cash', '2020-05-21', 1000, 2000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `prof_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dob` date DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` double DEFAULT NULL,
  `company_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_addr` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`prof_id`, `user_id`, `dob`, `city`, `state`, `address`, `pincode`, `company_name`, `office_addr`, `job_type`, `update_date`, `status`) VALUES
(1, 3, '2020-04-01', 'delhi', 'new delhi', 'Punjab', 110063, 'ar', 'rohini', 'private', '2020-05-20', 1),
(2, 4, '1999-06-15', 'delhi', 'new delhi', 'Punjabi Bagh', 110063, 'Raymonds', 'Pitam Pura', 'Private', '2020-05-03', 1),
(3, 5, '2013-06-22', 'delhi', 'new delhi', 'Punjabi Bagh', 110063, 'Raymonds', 'paschim vihar', 'private', '2020-05-05', 1),
(4, 6, '2000-09-01', 'delhi', 'new delhi', 'mayur vihar', 110063, 'lifestyle', 'pacific mall', 'Private', '2020-05-08', 1),
(5, 7, '2020-05-27', 'delhi', 'new delhi', 'Punjabi Bagh', 110063, 'bata', 'janak puri', 'Private', '2020-05-09', 1),
(6, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(7, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(8, 10, '2020-05-25', 'delhi', 'new delhi', 'paschim vihar', 110063, 'ar', 'mayur vihar', 'Private', '2020-05-12', 1),
(9, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(10, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(11, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(12, 1, '2020-05-11', 'delhi', 'new delhi', 'paschim vihar', 110063, 'AR', 'Patel Nagar', 'Private', '2020-05-20', 1),
(14, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `loan_application`
--
ALTER TABLE `loan_application`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `society_account`
--
ALTER TABLE `society_account`
  ADD PRIMARY KEY (`trans_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`acc_id`),
  ADD UNIQUE KEY `acc_no` (`acc_no`);

--
-- Indexes for table `user_fd_account`
--
ALTER TABLE `user_fd_account`
  ADD PRIMARY KEY (`fd_id`),
  ADD UNIQUE KEY `fd_accno` (`fd_accno`);

--
-- Indexes for table `user_payment`
--
ALTER TABLE `user_payment`
  ADD PRIMARY KEY (`pay_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`prof_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `loan_application`
--
ALTER TABLE `loan_application`
  MODIFY `app_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `society_account`
--
ALTER TABLE `society_account`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `user_fd_account`
--
ALTER TABLE `user_fd_account`
  MODIFY `fd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_payment`
--
ALTER TABLE `user_payment`
  MODIFY `pay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `prof_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
