<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>TRACK YOUR LOAN STATUS</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-3 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-9">

		<?php

$sql="select * from loan_application where user_id=".$_SESSION['user_id']." ORDER BY `loan_application`.`app_id` DESC";

$rs=mysqli_query($conn,$sql);

echo "<br>";

?>
<div class="table-responsive-sm">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Loan Amount</th>
        <th>Tenure</th>
        <th>Interest Rate</th>
        <th>Apply Date</th>
        <th>Response Date</th>
        <th>Status</th>

      </tr>
    </thead>
    <tbody>


<?php

$i=1;
while($row=mysqli_fetch_array($rs))

{

  

  echo "<tr>";
 	

 	echo "<td>$i</td>";
	echo "<td>".$row['loan_amt']."</td>";
	echo "<td>".$row['tenure']." months</td>";
	echo "<td>".$row['interest_rate']."%</td>";
	echo "<td>".$row['apply_date']."</td>";
  if ($row['approval_date']==NULL)
    echo "<td>NA</td>";
  else
    echo "<td>".$row['approval_date']."</td>";

   $status=$row['status'];
  if($status==2) $strStatus="Request Pending";
  if($status==0) $strStatus="Rejected";
  if($status==1) $strStatus="Approved";
	echo "<td>".$strStatus."</td>";
	
$i=$i+1;
  echo "</tr>";
  
}
  ?>
</tbody>
</table>
</div>


        
	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


