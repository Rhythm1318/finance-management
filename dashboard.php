<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<?php
	include("header.php");
	?>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-2 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-10">

		<?php

			echo "<h2>Welcome, ".$_SESSION['name']."</h2>";

			$sql="select last_activity,login_ip from users where user_id=".$_SESSION['user_id'];

$rs=mysqli_query($conn,$sql);
$cnt=mysqli_num_rows($rs);
while($row=mysqli_fetch_array($rs))
{	
	//echo "<div style='text-align:right'>";
	echo "Last Login: ".$row['last_activity'];
	echo "<br>Login IP: ".$row['login_ip'];
}

if($_SESSION['fd_account_no']!="")
{

	$sql="SELECT SUM(fd_amt) AS 'total fd_amt' FROM user_account WHERE user_id=".$_SESSION['user_id'];

	$rs=mysqli_query($conn,$sql);
	$cnt=mysqli_num_rows($rs);
	while($row=mysqli_fetch_array($rs))

		{
			  echo "<h3>"."Total FD Amount Deposit: "."<a href=listfd.php?totalfd=".$row['total fd_amt'].'>'.$row['total fd_amt']."</a></h3>";

		}
}

if($_SESSION['rd_account_no']!="")
{

	$sql="SELECT SUM(cur_bal) AS 'total rd_amt' FROM user_account WHERE acc_type='2' and user_id=".$_SESSION['user_id'];

	$rs=mysqli_query($conn,$sql);
	$cnt=mysqli_num_rows($rs);
	while($row=mysqli_fetch_array($rs))

		{
			  echo "<br>"."<h3>"."Total RD Amount Deposit: "."<a href=listrd.php?totalrd=".$row['total rd_amt'].'>'.$row['total rd_amt']."</a></h3>";

		}
}

if($_SESSION['loan_account_no']!="")
{

	$sql="SELECT SUM(loan_amt) AS 'total loan_amt' FROM user_account WHERE acc_type='3' and user_id=".$_SESSION['user_id'];

	$rs=mysqli_query($conn,$sql);
	$cnt=mysqli_num_rows($rs);
	while($row=mysqli_fetch_array($rs))

		{
			  echo "<br>"."<h3>"."Total Loan Amount: "."<a href=listloan.php?totalloan=".$row['total loan_amt'].'>'.$row['total loan_amt']."</a></h3>";

		}

}

		?>
	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


