<?php session_start(); include("chkAuth.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Change Password</title>
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
  
<div class="container-fluid">

<!-- header starts -->
<div class="row">

<div class="col-md-12">

<div class="jumbotron text-center">
    <h1>CHANGE PASSWORD</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
<!-- menu start here -->
<div class="col-md-3 bg-light">
<?php 
include("menu.php");
?>
	 
</div>
<!-- menu ends -->
<!-- content start -->
<div class="col-md-9bg-secondary"><b> Change Password</b>

<?php
  include("connect.php");

if(isset($_POST['submit']))
{
  
  $pass=$_POST['password'];
  $npass=$_POST['npass'];
  $cpass=$_POST['cpass'];
  $aid=$_SESSION['user_id'];
  

  $sql="select * from users where admin_id='$aid' and password='$pass'";
  
  $rs=mysqli_query($conn,$sql);

  $cnt=mysqli_num_rows($rs);

  if($cnt==0)
  {
    echo "sorry invalid password";
  }

  else
  {
    if($cpass!=$npass)
    {
      echo "New and Confirmed Password must be same";
    }
    else
    {
      $sql="update users set password=$npass where user_id=$uid";
      if(mysqli_query($conn,$sql))
      {
        echo "Password updated Successfully !!!!";
      }
      else
      {
        echo "There was some error ";
      }
    }
  }
}
else
  
{
?>  
  <form method="post" name="myform" action="">  
  <div class="form-group">
    <label for="password">Old Password: </label>
    <input name="password" type="password" class="form-control" placeholder="Enter Old Password" id="password" required>
  </div>
 <div class="form-group">
    <label for="npass">New Password: </label>
    <input name="npass" type="password" class="form-control" placeholder="Enter New Password" id="npass"required>
  </div>

  <div class="form-group">
    <label for="cpass">Confirmed Password: </label>
    <input name="cpass" type="password" class="form-control" placeholder="Enter Confirmed Password" id="cpass" required>
  </div>
    <button name="submit" type="submit" class="btn btn-primary btn-sm">Update Password</button>
  </form>

<?php
}
?>
	

</div>
<!-- content end -->
</div>
	<!-- Footer start here -->
<div class="row">
<div class="col-md-12">
</div>
</div>
<!-- footer end here -->
</div>
</body>
</html>
