<?php session_start(); include("chkAuth.php"); include("connect.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>

	

<div class="container-fluid">

<!-- header starts -->

<div class="row">

<div class="col-md-12">
	<div class="jumbotron text-center">
    <h1>MY FD ACCOUNTS</h1>
</div>

</div>
</div>
<!-- header end -->
<div class="row">
	
<!-- menu start here -->
<div class="col-md-3 bg-light">
	<?php
	include("menu.php");
	?>
</div>
	<div class="col-md-9">
<?php


  echo "<br>"."<div style='text-align:right'>"."<a href=create_fd_acc.php>Create new FD Account</a>";

		  
 $sql="SELECT * FROM `user_account` WHERE acc_type=1 and user_id=".$_SESSION['user_id']." ORDER BY `acc_id` DESC";

$rs=mysqli_query($conn,$sql);

?>
		 <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Account Number</th>
        <th>Amount Deposited</th>
        <th>Maturity Amount</th>
        <th>Tenure</th>
        <th>Account Status</th>

      </tr>
    </thead>
    <tbody>


<?php

$i=1;
while($row=mysqli_fetch_array($rs))

{
  echo "<tr>";
 	

 	echo "<td>$i</td>";

  echo "<td>"."<a href=myfdacc.php?accno=".$row['acc_no'].'>'.$row['acc_no']."</a></td>";
  echo "<td>".$row['fd_amt']."</td>";
  echo "<td>".$row['maturity_amt']."</td>";
  echo "<td>".$row['tenure']." months</td>";

  $status=$row['status'];
  if($status==0) $strStatus="Inactive";
  if($status==1) $strStatus="Active";
  echo "<td>".$strStatus."</td>";
  //echo "<br>";
$i=$i+1;
  echo "</tr>";
  
}
  ?>
</tbody>
</table>

<?php


?>
</div>

	</div>
</div>


<div class="row" >

<div class="col-md-12 bg-light mt-2">
<?php  include("footer.php");?>
</div>
</div>
</div>
</body>
</html>


